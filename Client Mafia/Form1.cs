﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using System.Net;
using System.IO;
using RestSharp;
using Newtonsoft.Json;

namespace Client_Mafia
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var client = new RestClient("http://127.0.0.1:3000/");
            var request = new RestRequest("api/v1/users", Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new User
            {
                login = textBox1.Text,
                password = textBox2.Text,
                name = textBox3.Text
            });
            User user = JsonConvert.DeserializeObject<User>(client.Execute(request).Content);
            if (user == null)
            {
                MessageBox.Show("Нет соединения с сервером");
                return;
            }
            if (user.error != null)
            {
                MessageBox.Show(user.error);
                return;
            }
            MessageBox.Show("Успешная регистрация");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var client = new RestClient("http://127.0.0.1:3000/");
            var request = new RestRequest("api/v1/users", Method.GET);
            request.AddParameter("login", textBox1.Text);
            request.AddParameter("password", textBox2.Text);
            User user = JsonConvert.DeserializeObject<User>(client.Execute(request).Content);
            if (user == null)
            {
                MessageBox.Show("Нет соединения с сервером");
                return;
            }
            if (user.error != null)
            {
                MessageBox.Show(user.error);
                return;
            }

            this.Hide();
            Main_menu newForm = new Main_menu(user.token, user.name);
            newForm.Show();
        }
    }
    public class User
    {
        [JsonProperty("name")]
        public string name { get; set; }

        [JsonProperty("login")]
        public string login { get; set; }

        [JsonProperty("password")]
        public string password { get; set; }

        [JsonProperty("token")]
        public string token { get; set; }

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("error")]

        public string error { get; set; }

    }

    public class Board
    {
        [JsonProperty("i")]
        public int i { get; set; }

        [JsonProperty("j")]
        public int j { get; set; }

        [JsonProperty("color")]
        public string color { get; set; }
    }

    public class GamePlace
    {
        [JsonProperty("white")]
        public int white { get; set; }

        [JsonProperty("black")]
        public int black { get; set; }

        [JsonProperty("board")]
        public List<Board> board { get; set; }
    }
    public class Room
    {
        [JsonProperty("name")] 
        public string name { get; set; }

        [JsonProperty("count_users")] 
        public string count_users { get; set; }

        [JsonProperty("game_place")]

        public GamePlace game_place { get; set; }

        [JsonProperty("id")]
        public int id { get; set; }

        [JsonProperty("error")]

        public string error { get; set; }

    }
    public class Users_room
    {
        [JsonProperty("error")]

        public string error { get; set; }

        [JsonProperty("name_room")]

        public string name_room { get; set; }

        [JsonProperty("user_name")]

        public string user_name { get; set; }

        [JsonProperty("color")]

        public string color { get; set; }

        [JsonProperty("state")]

        public string state { get; set; }
    }

}
