﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;

namespace Client_Mafia
{
    public partial class Main_menu : Form
    {
        string token;
        string user_name;
        static RestClient client = new RestClient("http://127.0.0.1:3000/");

        public Main_menu(string token, string name)
        {
            this.token = token;
            this.user_name = name;
            InitializeComponent();
            label2.Text = name;
            update_list_rooms();
            timer2.Enabled = true;
        }

        public void update_list_rooms()
        {
            listBox1.Items.Clear();
            var request = new RestRequest("api/v1/rooms", Method.GET);
            request.AddHeader("Token", token);
            var rooms = JsonConvert.DeserializeObject<List<Room>>(client.Execute(request).Content);
            for (int i = 0; i < rooms.Count; i++)
            {
                listBox1.Items.Add(rooms[i].name + " Игроков:" + rooms[i].count_users);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        { 
            var request = new RestRequest("api/v1/rooms/"+ textBox1.Text, Method.POST);
            request.AddHeader("Token", token);
            Room room = JsonConvert.DeserializeObject<Room>(client.Execute(request).Content);
            if (room.error != null)
            {
                MessageBox.Show(room.error);
                return;
            }

            MessageBox.Show(room.name);
            update_list_rooms();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            ListBox change = (ListBox)sender;
            if (change.SelectedItem == null) return;
            string str = change.SelectedItem.ToString();
            textBox1.Text = str.Remove(str.IndexOf(" Игроков"));

        }

        private void button3_Click(object sender, EventArgs e)
        {
            update_list_rooms();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "") return;
            var request = new RestRequest("api/v1/user_rooms", Method.POST);
            request.AddHeader("Token", token);
            request.AddParameter("name", textBox1.Text);
            Users_room user_room = JsonConvert.DeserializeObject<Users_room>(client.Execute(request).Content);
            if (user_room.error != null)
            {
                MessageBox.Show(user_room.error);
                return;
            }

            MessageBox.Show("Заходим в комнату "+user_room.name_room);
            this.Hide();
            Game_room newForm = new Game_room(user_room.name_room, token, user_name, this, user_room.color);
            newForm.Show();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            var request = new RestRequest("api/v1/users/ping", Method.POST);
            request.AddHeader("Token", token);
            client.Execute(request);
        }

        private void Main_menu_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }

}

