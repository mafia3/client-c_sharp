﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RestSharp;
using Newtonsoft.Json;
using System.IO;
using System.Reflection;

namespace Client_Mafia
{
    public partial class Game_room : Form
    {
        string token;
        string room_name;
        string user_name;
        string user_color;
        
        string[,] pole;
        int white_score;
        int black_score;
        int[] state_step_white_i;
        int[] state_step_j;
        int[] state_step_black_i;
        int current_i = 0;
        int current_j = 0;
        PictureBox[] pictures;
        Main_menu main_menu_form;
        
        static RestClient client = new RestClient("http://127.0.0.1:3000/");
        public Game_room(string room_name, string token, string user_name, Main_menu newForm, string user_color)
        {
            this.token = token;
            this.room_name = room_name;
            this.user_name = user_name;
            this.main_menu_form = newForm;
            this.user_color = user_color;
            InitializeComponent();
            label1.Text = user_name;
            label3.Text = user_color;
            white_score = 0;
            black_score = 0;
            this.Text = room_name;
            timer1.Enabled = true;
            pole = new string[9, 9];
            state_step_white_i = new int[4] { -1, -1, -2, -2 };
            state_step_black_i = new int[4] { +1, +1, +2, +2 };
            state_step_j = new int[4] { -1, +1, -2, +2 };
            pictures = this.Controls.OfType<PictureBox>()
            .Where(x => x.Name.StartsWith("pictureBox"))
            .ToArray();
            get_rooms();
            draw_pictures();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var request = new RestRequest("api/v1/user_rooms", Method.PATCH);
            request.AddHeader("Token", token);
            switch (button1.Text)
            {
                case "Не готов":
                    request.AddParameter("state", "wait");
                    button1.Text = "Готов";
                    break;
                case "Готов":
                    request.AddParameter("state", "ready");
                    button1.Text = "Не готов";
                    break;
            }
            Users_room user_room = JsonConvert.DeserializeObject<Users_room>(client.Execute(request).Content);
            if (user_room.error != null)
            {
                MessageBox.Show(user_room.error);
                return;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var request = new RestRequest("api/v1/user_rooms", Method.DELETE);
            request.AddHeader("Token", token);
            Users_room user_room = JsonConvert.DeserializeObject<Users_room>(client.Execute(request).Content);
            if (user_room != null && user_room.error != null)
            {
                MessageBox.Show(user_room.error);
                return;
            }

            this.Close();
            main_menu_form.Show();

        }
        public Image image_white = Image.FromFile
   (Directory.GetCurrentDirectory()
   + @"\Sprites\white.png");

        public Image image_black = Image.FromFile
   (Directory.GetCurrentDirectory()
   + @"\Sprites\black.png");
        public void draw_pictures()
        {
            int pic = 0;
            for (int i = 1; i <= 8; i++)
                for (int j = 1; j <= 8; j++)
                {
                    if (pole[i, j] != null)
                    {
                        if (pole[i, j] == "white") pictures[pic].BackgroundImage = image_white;
                        if (pole[i, j] == "black") pictures[pic].BackgroundImage = image_black;
                        pictures[pic].Location = new Point(56 * j - 56 + 213, 56 * i - 56 + 37);
                        pic++;
                    }
                }
        }

        
        void get_rooms()
        {
            var request = new RestRequest("api/v1/rooms/" + room_name, Method.GET);
            request.AddHeader("Token", token);
            var str = client.Execute(request).Content;
            var room = JsonConvert.DeserializeObject<Room>(client.Execute(request).Content);

            if (user_color == "black")
            {

                label5.Text = "Очки: " + room.game_place.black.ToString();
                label6.Text = "Очки: " + room.game_place.white.ToString();
            }
            else
            {
                label6.Text = "Очки: " + room.game_place.black.ToString();
                label5.Text = "Очки: " + room.game_place.white.ToString();
            }

            white_score = room.game_place.white;
            black_score = room.game_place.black;
            for (int i = 0; i <= room.game_place.board.Count - 1; i++)
            {
                pole[room.game_place.board[i].i, room.game_place.board[i].j] = room.game_place.board[i].color;
            }
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            var request = new RestRequest("api/v1/user_rooms", Method.GET);
            request.AddHeader("Token", token);
            string str = client.Execute(request).Content;
            var user_rooms = JsonConvert.DeserializeObject<List<Users_room>>(client.Execute(request).Content);
            if (user_rooms.Count >= 2)
            {
                for (int i = 0; i < user_rooms.Count; i++)
                {
                    if (user_rooms[i].user_name != user_name)
                    {
                        label2.Text = user_rooms[i].user_name;
                        label4.Text = user_rooms[i].color;
                    }
                }
            }
            else
            {
                label2.Text = "Противника нет";
                label4.Text = "Цвет";
            }
            get_rooms();
            draw_pictures();

            for (int i = 0; i < user_rooms.Count; i++)
            {
                if (user_rooms[i].state == "wait")
                {
                    button1.Enabled = true;
                    button1.Text = "Готов";
                    return;
                }
            }
            button1.Enabled = false;
        }

        private void Game_room_FormClosed(object sender, FormClosedEventArgs e)
        {
            var request = new RestRequest("api/v1/user_rooms", Method.DELETE);
            request.AddHeader("Token", token);
            Users_room user_room = JsonConvert.DeserializeObject<Users_room>(client.Execute(request).Content);
            if (user_room != null && user_room.error != null)
            {
                MessageBox.Show(user_room.error);
                return;
            }

            main_menu_form.Show();
        }

        // Выделение пешки
        private void pictureBox_Click(object sender, EventArgs e)
        {
            var request = new RestRequest("api/v1/user_rooms", Method.GET);
            request.AddHeader("Token", token);
            string str = client.Execute(request).Content;
            var user_rooms = JsonConvert.DeserializeObject<List<Users_room>>(client.Execute(request).Content);

            /*if (user_rooms.Count != 2) return;

            for (int i=0;i< user_rooms.Count;i++)
            {
                if (user_rooms[i].state == "wait") return;
            }

            button1.Enabled = false;*/


            PictureBox cp = (PictureBox)sender;
            current_i = (cp.Location.Y- 37) / 56 + 1;
            current_j = (cp.Location.X- 213) / 56 + 1;
            if (user_color != pole[current_i, current_j])
            {
                current_i = 0;
                current_j = 0;
            }
        }
            //state_step_white_i = new int[4] { -1, -1, -2, -2 };
            //state_step_black_i = new int[4] { +1, +1, +2, +2 };
           // state_step_j = new int[4] { -1, +1, -2, +2 };
        bool check_traectory(string color, int current_i, int current_j, int step_i, int step_j)
        {
            //TODO проверка хода, если дамка
            for (int i = 0; i <= 3; i++)
                if (color == "white")
                {
                    if ((step_i - current_i == state_step_white_i[i]) && (step_j - current_j == state_step_j[i])) return true;
                }
                else
                {
                    if ((step_i - current_i == state_step_black_i[i]) && (step_j - current_j == state_step_j[i])) return true;
                }

            return false;
        }

        bool check_eating(string color, int current_i, int current_j, int step_i, int step_j)
        {
            if ((step_i < current_i))
            {
                if (step_j < current_j)
                {
                    int j = current_j - 1;
                    bool f = false;
                    for (int i = current_i - 1; i > step_i; i--)
                    {
                        //TODO Добавить проверку на дамку
                        if ((pole[i, j] == null)) return false;
                        if (pole[i, j] == color) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null) && (f)) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null)) f = true;
                        j--;
                    }
                }
                else
                {
                    int j = current_j + 1;
                    bool f = false;
                    for (int i = current_i - 1; i > step_i; i--)
                    {
                        //TODO Добавить проверку на дамку
                        if ((pole[i, j] == null)) return false;
                        if (pole[i, j] == color) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null) && (f)) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null)) f = true;
                        j++;
                    }
                }
            }
            else
            {
                if (step_j < current_j)
                {
                    int j = current_j - 1;
                    bool f = false;
                    for (int i = current_i + 1; i < step_i; i++)
                    {
                        //TODO Добавить проверку на дамку
                        if ((pole[i, j] == null)) return false;
                        if (pole[i, j] == color) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null) && (f)) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null)) f = true;
                        j--;
                    }
                }
                else
                {
                    int j = current_j + 1;
                    bool f = false;
                    for (int i = current_i + 1; i < step_i; i++)
                    {
                        //TODO Добавить проверку на дамку
                        if ((pole[i, j] == null)) return false;
                        if (pole[i, j] == color) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null) && (f)) return false;
                        if ((pole[i, j] != color) && (pole[i, j] != null)) f = true;
                        j++;
                    }
                }
            }


            return true;
        }

        int eating(string color, int current_i, int current_j, int step_i, int step_j)
        {
            int result = 0;
            if ((step_i < current_i))
            {
                if (step_j < current_j)
                {
                    int j = current_j - 1;
                    for (int i = current_i - 1; i > step_i; i--)
                    {
                        if ((pole[i, j] != color) && (pole[i, j] != null)) result++;
                        pole[i, j] = null;
                        j--;
                    }
                }
                else
                {
                    int j = current_j + 1;
                    for (int i = current_i - 1; i > step_i; i--)
                    {
                        if ((pole[i, j] != color) && (pole[i, j] != null)) result++;
                        pole[i, j] = null;
                        j++;
                    }
                }
            }
            else
            {
                if (step_j < current_j)
                {
                    int j = current_j - 1;
                    for (int i = current_i + 1; i < step_i; i++)
                    {
                        if ((pole[i, j] != color) && (pole[i, j] != null)) result++;
                        pole[i, j] = null;
                        j--;
                    }
                }
                else
                {
                    int j = current_j + 1;
                    for (int i = current_i + 1; i < step_i; i++)
                    {
                        if ((pole[i, j] != color) && (pole[i,j] != null)) result++;
                        pole[i, j] = null;
                        j++;
                    }
                }
            }

            return result;
        }


        bool check_step(string color, int current_i, int current_j, int step_i , int step_j)
        {
            if (!check_traectory(color, current_i, current_j, step_i, step_j)) return false;

            if (!check_eating(color, current_i, current_j, step_i, step_j)) return false;

            return true;
        }

        //Ход пешек
        private void pictureBox1_Click(object sender, EventArgs e)
        {

            MouseEventArgs ms = (MouseEventArgs)e;
            if ((ms.X < 25) || (ms.Y < 25) || (ms.X > 473) || (ms.Y > 473)) return;

            if (current_i == 0) return;
            if (current_j == 0) return;

            int j = (ms.X - 25) / 56+1;
            int i = (ms.Y - 25) / 56+1;

            if ((current_i == i) && (current_j == j)) return;

            //проверка хода
            string color = pole[current_i, current_j];
            if (!check_step(color, current_i, current_j, i, j)) return;
            pole[current_i, current_j] = null;
            List<Board> board = new List<Board>();
            int count_eat = eating(color, current_i, current_j, i, j);

            pole[i, j] = color;
            var request = new RestRequest("api/v1/rooms/"+room_name+"/step", Method.PATCH);
            for (int i_ = 1; i_ <= 8; i_++)
                for (int j_ = 1; j_ <= 8;j_++)
                {
                    board.Add(new Board{ 
                        i= i_,
                        j= j_,
                        color = pole[i_,j_]});
                }

            GamePlace game_place = new GamePlace();
            game_place.board = board;
            game_place.white = white_score;
            game_place.black = black_score;
            if (color == "white")
            {
                game_place.white += count_eat;
            }
            else
            {
                game_place.black += count_eat;
            }
            var str = JsonConvert.SerializeObject(board);

            request.AddHeader("Token", token);
            request.AddJsonBody(new Room { game_place = game_place });

            Room user_room = JsonConvert.DeserializeObject<Room>(client.Execute(request).Content);
            if (user_room != null && user_room.error != null)
            {
                MessageBox.Show(user_room.error);
                return;
            }
            draw_pictures();
        }
    }
}
